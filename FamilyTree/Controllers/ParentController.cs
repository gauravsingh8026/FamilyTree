﻿namespace FamilyTree.Controllers
{
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Models;
    using System.Collections.Generic;
    using System.Linq;
    using FamilyTree.Models.MSSQL;
    public class ParentController : Controller
    {
        public FamilyTreeEntities db = new FamilyTreeEntities();
        private static readonly string collectionId = "Parent";
        [ActionName("Index")]
        public async Task<ActionResult> IndexAsync()
        {
            var items = await DocumentDBRepository<Parent>.GetItemsAsync(collectionId);
            return View(items);
        }

#pragma warning disable 1998
        [ActionName("Create")]
        public async Task<ActionResult> CreateAsync()
        {
            return View();
        }
#pragma warning restore 1998

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync([Bind(Include = "Id,ParentId,Name")] Parent item)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<Parent> items = await DocumentDBRepository<Parent>.GetItemsAsync(collectionId);
                var ParentId = items.Max(p => p.ParentId);
                item.ParentId = ParentId + 1;
                var result = await DocumentDBRepository<Parent>.CreateItemAsync(item, collectionId);
                
                ParentM parentM = new ParentM()
                {
                    id = result.Id,
                    parentid = item.ParentId,
                    name = item.Name
                };
                db.ParentMs.Add(parentM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync([Bind(Include = "Id,Name,Description,Completed")] Parent item)
        {
            if (ModelState.IsValid)
            {
                await DocumentDBRepository<Parent>.UpdateItemAsync(item.Id, item, collectionId);
                return RedirectToAction("Index");
            }

            return View(item);
        }

        [ActionName("Edit")]
        public async Task<ActionResult> EditAsync(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Parent item = await DocumentDBRepository<Parent>.GetItemAsync(id, collectionId);
            if (item == null)
            {
                return HttpNotFound();
            }

            return View(item);
        }

        [ActionName("Delete")]
        public async Task<ActionResult> DeleteAsync(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Parent item = await DocumentDBRepository<Parent>.GetItemAsync(id, collectionId);
            if (item == null)
            {
                return HttpNotFound();
            }

            return View(item);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmedAsync([Bind(Include = "Id")] string id)
        {
            await DocumentDBRepository<Parent>.DeleteItemAsync(id, collectionId);
            return RedirectToAction("Index");
        }

        [ActionName("Details")]
        public async Task<ActionResult> DetailsAsync(string id)
        {
            Parent item = await DocumentDBRepository<Parent>.GetItemAsync(id, collectionId);
            return View(item);
        }
    }
}