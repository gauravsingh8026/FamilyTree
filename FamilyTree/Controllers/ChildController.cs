﻿namespace FamilyTree.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Models;
    using System.Linq;
    using FamilyTree.Models.MSSQL;
    public class ChildController : Controller
    {
        public FamilyTreeEntities db = new FamilyTreeEntities();
        private static readonly string collectionId = "Child";
        [ActionName("Index")]
        public async Task<ActionResult> IndexAsync()
        {
            ChildModel childModel = new ChildModel();
            var items = await DocumentDBRepository<Child>.GetItemsAsync(collectionId);

            childModel.children = items.ToList();
            return View(childModel);
        }

#pragma warning disable 1998
        [ActionName("Create")]
        public async Task<ActionResult> CreateAsync()
        {
            var parentItems = await DocumentDBRepository<Parent>.GetItemsAsync("Parent");
            Child child = new Child();
            child.ParentList = parentItems;
            return View(child);
        }
#pragma warning restore 1998

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync([Bind(Include = "Id,ChildId,ParentId,Name")] Child item)
        {
            if (ModelState.IsValid)
            {
                //item.Id = "1";
                IEnumerable<Child> items = await DocumentDBRepository<Child>.GetItemsAsync(collectionId);
                var ChildId = 0;
                if(items != null && items.Count() > 0)
                    ChildId = items.Max(p => p.ChildId);
                //item.Id = "1";
                item.ChildId = ChildId + 1;
                var result = await DocumentDBRepository<Child>.CreateItemAsync(item, collectionId);
                ChildM childM = new ChildM() {
                    id = result.Id,
                    childid = item.ChildId,
                    parentid = item.ParentId,
                    name = item.Name
                };
                db.ChildMs.Add(childM);
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }

            return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync([Bind(Include = "Id,ParentId,Name")] Child item)
        {
            if (ModelState.IsValid)
            {
                await DocumentDBRepository<Child>.UpdateItemAsync(item.Id, item, collectionId);
                return RedirectToAction("Index");
            }

            return View(item);
        }

        [ActionName("Edit")]
        public async Task<ActionResult> EditAsync(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Child item = await DocumentDBRepository<Child>.GetItemAsync(id, collectionId);
            if (item == null)
            {
                return HttpNotFound();
            }

            return View(item);
        }

        [ActionName("Delete")]
        public async Task<ActionResult> DeleteAsync(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Child item = await DocumentDBRepository<Child>.GetItemAsync(id, collectionId);
            if (item == null)
            {
                return HttpNotFound();
            }

            return View(item);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmedAsync([Bind(Include = "Id")] string id)
        {
            await DocumentDBRepository<Parent>.DeleteItemAsync(id, collectionId);
            return RedirectToAction("Index");
        }

        [ActionName("Details")]
        public async Task<ActionResult> DetailsAsync(string id)
        {
            Child item = await DocumentDBRepository<Child>.GetItemAsync(id, collectionId);
            return View(item);
        }
    }
}