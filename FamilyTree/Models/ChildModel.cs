﻿using FamilyTree.Models.MSSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
namespace FamilyTree.Models
{
    public class ChildModel
    {
        //public FamilyTreeEntities db = new FamilyTreeEntities();
        public List<Child> children = new List<Child>();
        public Child child = new Child();
        public async Task<string> GetParent(int parentid)
        {
            var parentItems = await DocumentDBRepository<Parent>.GetItemsAsync(d => d.ParentId == parentid,"Parent");
            return parentItems.FirstOrDefault().Name;
            return "";
        }
    }
}