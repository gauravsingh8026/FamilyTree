﻿namespace FamilyTree.Models
{
    using Microsoft.Azure.Documents;
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class Child
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "childid")]
        public int ChildId { get; set; }

        [JsonProperty(PropertyName = "parentid")]
        public int ParentId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        public IEnumerable<Parent> ParentList { get; set; }

        public virtual Parent daddy { get; set; }

       
    }
}