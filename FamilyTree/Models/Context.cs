﻿namespace FamilyTree.Models
{
    using System.Data.Entity;

    public class Context : DbContext
    {
        public Context() : base("name=Context")
        {
        }

        public DbSet<Parent> Parents { get; set; }
        public DbSet<Child> Childs { get; set; }
    }
}
